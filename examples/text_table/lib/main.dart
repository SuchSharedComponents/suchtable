import 'package:flutter/material.dart';
import 'package:text_table/table.dart';

void main() {
  runApp(new MyApp());
}

const _showPerformanceOverlay = !true;

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return new MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData.light(),
        darkTheme: ThemeData.dark(),
        themeMode: ThemeMode.system,
        debugShowCheckedModeBanner: false,
        showPerformanceOverlay: _showPerformanceOverlay,
        home: SafeArea(
            child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(height: _showPerformanceOverlay ? 180.0 : 8.0),
            Text(
              'Пример текстовой таблицы',
              style: TextStyle(
                color: theme.colorScheme.onBackground,
                fontSize: 21.0,
                fontWeight: FontWeight.w300,
                decoration: TextDecoration.none,
              ),
            ),
            SizedBox(height: 24.0),
            DemoTextTable(),
          ],
        )));
  }
}
