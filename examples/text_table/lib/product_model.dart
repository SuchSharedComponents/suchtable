import 'dart:math';

/// демо класс данных
class ProductModel {
  const ProductModel(
    this.id, {
    required this.title,
    this.category = '',
    this.unit = '',
    this.selfCost = 0,
    this.tax = 0,
    this.markup = 0,
  });

  factory ProductModel.random(int id) {
    final rng = Random();
    final titles = [
      'Вода',
      'Сок',
      'Квас',
      'Дюшес',
      'Лимонад',
      'Пиво',
      'Тархун',
      'Сидр',
      'Швепс',
      'Coca-Cola',
      'Red Bull Blue Edition',
      'Black Monster'
    ];
    final title = titles[rng.nextInt(titles.length)];
    return ProductModel(
      id,
      title: title,
      category: 'Охлаждённые напитки',
      unit: 'бут.',
      selfCost: (rng.nextDouble() * 150).ceilToDouble(),
      tax: 0,
      markup: (rng.nextDouble() * 50).ceilToDouble(),
    );
  }

  final int id;
  final String title;
  final String category;
  final String unit;
  final double selfCost;
  final double tax;
  final double markup;
}
