import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:such_table/such_table.dart';
import 'package:text_table/product_model.dart';

class DemoTextTable extends HookWidget {
  const DemoTextTable();

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    // "загружаем" данные
    final isLoading = useState(true);
    final data = useState<List<ProductModel>>([]);
    if (data.value.isEmpty) {
      Future.delayed(Duration(seconds: 3), () {
        for (var i = 0; i < 33; i++) {
          data.value.add(ProductModel.random(i));
        }
        isLoading.value = false;
      });
    }

    final defaultTextStyle = TextStyle(
      color: theme.colorScheme.onBackground,
      fontWeight: FontWeight.w300,
      fontSize: 13.0,
      decoration: TextDecoration.none,
    );

    // рисуем таблицу
    return SuchTable<ProductModel>(
      'products',
      stickyHeader: true,
      headers: [
        SuchCellData(
          '№',
          cellStyle: SuchContainerStyle(
            width: 600,
          ),
        ),
        SuchCellData('Наименование',
            cellStyle: SuchContainerStyle(maxWidth: 190)),
        SuchCellData('Категория', cellStyle: SuchContainerStyle(maxWidth: 190)),
        SuchCellData('Ед. изм.', cellStyle: SuchContainerStyle(maxWidth: 60)),
        SuchCellData('Себестоимость',
            cellStyle: SuchContainerStyle(maxWidth: 130)),
        SuchCellData('Налог', cellStyle: SuchContainerStyle(maxWidth: 50)),
        SuchCellData('Наценка', cellStyle: SuchContainerStyle(maxWidth: 70)),
        SuchCellData('Цена', cellStyle: SuchContainerStyle(maxWidth: 70)),
      ],
      data: data.value,
      isLoading: isLoading.value,
      rowBuilder: (item, index) => SuchRowData(
        cells: [
          SuchCellData(
            item.id.toString(),
            cellStyle: SuchContainerStyle(
              width: 600,
            ),
          ),
          SuchCellData(
            item.title,
            // изменение стиля отдельных ячеек:
            cellStyle: SuchContainerStyle(
                textStyle: defaultTextStyle.copyWith(color: Colors.green)),
          ),
          SuchCellData(item.category),
          SuchCellData(item.unit),
          SuchCellData(item.selfCost.toStringAsFixed(2)),
          SuchCellData(item.tax.toStringAsFixed(2)),
          SuchCellData(item.markup.toStringAsFixed(2)),
          SuchCellData(((item.selfCost + item.markup) * (1 + (item.tax / 100)))
              .toStringAsFixed(2)),
        ],
        // изменение стиля конкретной строчки по условию:
        cellStyle: SuchContainerStyle(
          opacity: item.selfCost < 22 ? .5 : 1.0,
          useDynamicCellWidth: false,
        ),
      ),
      headerStyle: SuchContainerStyle(
        textStyle: defaultTextStyle.copyWith(fontWeight: FontWeight.bold),
        padding: const EdgeInsets.symmetric(vertical: 3.0, horizontal: 11.0),
        useDynamicCellWidth: false,
      ),
      cellStyle: SuchContainerStyle(
          useDynamicCellWidth: false,
          textStyle: defaultTextStyle,
          textAlign: TextAlign.center,
          padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 7.0),
          duration: Duration(milliseconds: 888),
          height: 23,
          shimmerColors: [theme.colorScheme.onBackground, theme.primaryColor],
          shimmerBoxDecoration: BoxDecoration(
            color: Colors.grey,
            borderRadius: BorderRadius.circular(5),
          )),
    );
  }
}
