import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:shimmer/shimmer.dart';

import '../../such_table.dart';

/// виджет ячеек
class SuchCell extends HookWidget {
  const SuchCell({
    required this.width,
    required this.height,
    required this.data,
    required this.cellStyle,
    required this.onCellSize,
    required this.x,
    required this.y,
    this.sortDescending,
    Key? key,
  }) : super(key: key);
  final ValueNotifier<double?> width;
  final ValueNotifier<double?> height;
  final SuchContainerStyle cellStyle;
  final SuchCellData data;

  /// показывать стрелочку сортировки в порядке убывания
  /// [true]  🔽
  /// [null]
  /// [false] 🔼
  final bool? sortDescending;

  /// координаты
  final int x;
  final int y;

  /// функция, реагирующая на изменения размеров ячеек
  final Function(int x, int y, Size size) onCellSize;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    // print('cell $x x $y (re)build, size: ${width.value} x ${height.value}');
    // объединяем стиль для ячейки
    final computedCellStyle =
        data.cellStyle != null ? data.cellStyle! | cellStyle : cellStyle;
    // текстовая ячейка?
    final isTextCell = data.child == null;
    // подписываемся на обновления размеров
    useListenable(width);
    useListenable(height);
    // текст
    final text = data.text ?? '';
    // проверяем пришли ли точные размеры для столбца/строчки
    // считаем размеры содержимого
    final computedSize = cellStyle.useDynamicCellWidth
        ? isTextCell == true
            // для текстовых ячеек считаем размер текста
            ? getContainerSizeWithText(text, computedCellStyle,
                withPadding: true)
            // для ячеек с виджетом берем заданый размер из объединенного стиля
            : Size(
                computedCellStyle.width ?? computedCellStyle.maxWidth ?? 100.0,
                computedCellStyle.height ?? computedCellStyle.maxHeight ?? 35.0,
              )
        : Size(
            data.cellStyle?.width ?? 100.0,
            cellStyle.height ?? 35.0,
          );
    // сообщаем размер в обработчик для вычисления общей ширины/высоты столбцов/строк
    if (!data.isLoading) onCellSize(x, y, computedSize);
    // пока нет общих размеров, задаем размеры ячейки из посчитанных
    Size? cellSize = Size(
        width.value ?? computedSize.width, height.value ?? computedSize.height);
    if (data.isLoading) {
      if (cellStyle.useDynamicCellWidth) {
        cellSize = Size(
            max(computedCellStyle.maxWidth ?? 100, width.value ?? 0),
            computedCellStyle.height ?? 25);
      } else {
        cellSize = Size(max(data.cellStyle?.width ?? 100, width.value ?? 0),
            computedCellStyle.height ?? 25);
      }
      onCellSize(x, y, cellSize);
    }
    // создаем контейнер с посчитанными или заданными размерами
    return AnimatedOpacity(
      opacity: width.value == null ? .0 : computedCellStyle.opacity ?? 1.0,
      duration: computedCellStyle.duration ?? Duration.zero,
      curve: computedCellStyle.curve ?? Curves.easeInOut,
      key: data.key,
      child: AnimatedContainer(
        duration: computedCellStyle.duration ?? Duration(milliseconds: 111),
        curve: computedCellStyle.curve ?? Curves.easeInOut,
        width: cellStyle.useDynamicCellWidth
            ? cellSize.width
            : data.cellStyle?.width ?? 100,
        height: cellSize.height,
        padding: computedCellStyle.padding,
        alignment: computedCellStyle.align,
        decoration: computedCellStyle.decoration ?? BoxDecoration(),
        child: data.isLoading
            ? Shimmer.fromColors(
                baseColor: computedCellStyle.shimmerColors?[0] ??
                    theme.colorScheme.surface,
                highlightColor: computedCellStyle.shimmerColors?[1] ??
                    theme.colorScheme.primary,
                child: Container(
                  width: cellStyle.useDynamicCellWidth
                      ? cellSize.width
                      : data.cellStyle?.width ?? 100,
                  height: cellSize.height,
                  decoration: computedCellStyle.shimmerBoxDecoration,
                ),
              )
            : SuchText(text, style: computedCellStyle),
      ),
    );
  }
}
