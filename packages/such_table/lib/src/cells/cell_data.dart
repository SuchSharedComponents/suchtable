import 'package:flutter/material.dart';

import '../../such_table.dart';

/// данные ячейки таблицы SuchTable
class SuchCellData {
  const SuchCellData(
    this.text, {
    this.child,
    this.onTap,
    this.cellStyle,
    this.sortAsNumber,
    this.sortAsString,
    this.isLoading = false,
    Key? this.key,
  });

  /// текст ячейки
  final String? text;

  /// виджет вместо текста ячейки.
  /// если указан виджет, ширина столбца должна быть указана через maxWidth
  /// с учётом отступов padding: [100] = 10 + 80 + 10
  final Widget? child;

  final VoidCallback? onTap;

  final bool isLoading;

  /// данные для сортировки по числу
  /// Используется когда для столбца выбран тип сортировки по числу.
  final num? sortAsNumber;

  /// Данные для сортировки по тексту.
  /// Требуется когда вместо текста отображается виджет
  final String? sortAsString;

  /// данные для замены оформления стиля конкретной ячейки
  final SuchContainerStyle? cellStyle;

  /// (опц.) индивидуальный ключ для нахождения ячейки
  final Key? key;
}
