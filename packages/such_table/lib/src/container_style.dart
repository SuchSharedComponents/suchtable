import 'package:flutter/material.dart';

/// общие параметры для построения контейнеров
/// все параметры должны быть опциональные со значениями по умолчанию в компонентах
class SuchContainerStyle {
  const SuchContainerStyle({
    this.padding,
    this.align,
    this.width,
    this.height,
    this.maxWidth,
    this.useDynamicCellWidth = true,
    this.maxHeight,
    this.decoration,
    this.opacity = 1.0,
    this.duration,
    this.curve,
    this.selectableText,
    this.textStyle,
    this.textAlign,
    this.nullPlaceholder = 'ø', // выводится когда содержимое == null
    this.shimmerColors,
    this.shimmerBoxDecoration,
  });

  // Определять ли ширину ячейки в зависимости от контента
  final bool useDynamicCellWidth;

  /// отступы контейнера
  final EdgeInsets? padding;

  /// выравнивание содержимого (по умолчанию – по центру)
  final Alignment? align;

  /// заданная ширина
  final double? width;

  /// заданная высота
  final double? height;

  /// максимальная допустимая ширина контейнера
  final double? maxWidth;

  /// максимальная допустимая высота контейнера
  final double? maxHeight;

  /// стиль контейнеров
  final BoxDecoration? decoration;

  /// [0.0 - 1.0] непрозрачность
  final double? opacity;

  /// продолжительность анимаций
  final Duration? duration;

  /// кривая анимации
  final Curve? curve;

  /// использовать выделяющийся текст
  final bool? selectableText;

  /// стиль текста
  final TextStyle? textStyle;

  /// горизонтальное выравнивание текста внутри контейнера
  final TextAlign? textAlign;

  /// текст, выводимый вместо значений null
  final String nullPlaceholder;

  /// два цвета для генерации градиента шиммера
  /// первый – baseColor
  final List<Color>? shimmerColors;
  final BoxDecoration? shimmerBoxDecoration;

  SuchContainerStyle operator |(SuchContainerStyle? other) =>
      SuchContainerStyle(
        useDynamicCellWidth: useDynamicCellWidth,
        align: align ?? other?.align,
        width: width ?? other?.width,
        height: height ?? other?.height,
        maxWidth: maxWidth ?? other?.maxWidth,
        maxHeight: maxHeight ?? other?.maxHeight,
        decoration: decoration ?? other?.decoration,
        opacity: opacity ?? other?.opacity,
        duration: duration ?? other?.duration,
        curve: curve ?? other?.curve,
        padding: padding ?? other?.padding,
        selectableText: selectableText ?? other?.selectableText,
        textAlign: textAlign ?? other?.textAlign,
        textStyle: textStyle ?? other?.textStyle,
        shimmerColors: shimmerColors ?? other?.shimmerColors,
        shimmerBoxDecoration:
            shimmerBoxDecoration ?? other?.shimmerBoxDecoration,
      );
}
