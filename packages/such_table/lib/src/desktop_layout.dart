import 'package:flutter/material.dart';
import 'package:such_table/src/rows/row.dart';

import '../such_table.dart';

/// layout для отображения таблиц на десктопе, в браузере или на планшете (с достаточной шириной экрана)
class SuchTableDesktopLayout<T> extends StatefulWidget {
  SuchTableDesktopLayout({
    required this.headers,
    required this.isLoading,
    this.data = const [],
    this.rowBuilder,
    required this.stickyHeader,
    required this.availableWidth,
    required this.shimmerRows,
    required this.height,
    required this.cellStyle,
    this.headerStyle,
    Key? key,
  }) : super(key: key);

  /// список заголовков таблицы
  final List<SuchCellData> headers;

  /// данные для таблицы
  final List<T?> data;
  final bool isLoading;

  final SuchRowData? Function(T item, int index)? rowBuilder;

  final bool stickyHeader;

  /// доступная ширина (constraints)
  final double availableWidth;
  // высота таблицы
  final double height;
  final int shimmerRows;

  /// общий стиль всех ячеек ячейки (включая незаданные поля у [headerStyle])
  final SuchContainerStyle cellStyle;

  /// отдельные параметры для всех ячеек заголовка
  /// Для незаданных полей используются соответствующие параметры из [cellStyle]
  final SuchContainerStyle? headerStyle;

  @override
  State<SuchTableDesktopLayout<T>> createState() =>
      _SuchTableDesktopLayoutState<T>();
}

class _SuchTableDesktopLayoutState<T> extends State<SuchTableDesktopLayout<T>> {
  @override
  Widget build(BuildContext context) {
    _init(widget.headers.length,
        widget.isLoading ? widget.shimmerRows + 1 : widget.data.length + 1);
    final List<SuchTableRow> rows = [];
    final tableHeader = SuchTableRow(
      y: 0,
      rowData: SuchRowData(cells: widget.headers),
      columnWidths: _columnWidths,
      height: _rowHeights[0], // 0 строка всегда заголовок
      onCellSize: _onCellSize,
      generalStyle: widget.headerStyle != null
          ? widget.headerStyle! | widget.cellStyle
          : widget.cellStyle,
    );
    if (widget.stickyHeader == false) {
      rows.add(tableHeader);
    }
    rows.addAll(_buildRows(widget.headers.length));
    return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (widget.stickyHeader == true) tableHeader,
          Container(
            height: widget.stickyHeader
                ? widget.height - (_rowHeights[0].value ?? 30.0)
                : widget.height,
            child: ListView(
              children: rows,
            ),
          ),
        ]);
  }

  /// билдит все строчки данных таблицы
  List<SuchTableRow> _buildRows(int maxWideLength) {
    final List<SuchTableRow> result = [];
    if (widget.isLoading) {
      _ensureListLength(
          _rowHeights, widget.shimmerRows + 1, (p0) => ValueNotifier(null));
      for (var i = 0; i < widget.shimmerRows; i++) {
        final rowData = SuchRowData(
            cells: List.generate(
                maxWideLength,
                (index) => SuchCellData('loading',
                    cellStyle: SuchContainerStyle(
                      width: widget.headers[index].cellStyle?.width ?? 100,
                      maxWidth:
                          widget.headers[index].cellStyle?.maxWidth ?? 100,
                    ),
                    isLoading: true)));
        result.add(
          SuchTableRow(
              rowData: rowData,
              onCellSize: _onCellSize,
              columnWidths: _columnWidths,
              height: _rowHeights[0],
              y: i + 1,
              generalStyle: widget.cellStyle),
        );
      }
      return result;
    }
    for (var y = 1; y <= widget.data.length; y++) {
      // индекс в widget.data
      final index = y - 1;
      final data = widget.data[index];
      if (data == null) continue;
      final SuchRowData? rowData = widget.rowBuilder!(data, index);
      if (rowData == null) continue;
      result.add(
        SuchTableRow(
          y: y,
          rowData: rowData,
          onCellSize: _onCellSize,
          columnWidths: _columnWidths,
          height: _rowHeights[y],
          generalStyle: widget.cellStyle,
        ),
      );
    }
    return result;
  }

  /// хранимые размеры каждой ячейки.
  /// Для динамического подгона не только по максимальной ширине за всё время жизни,
  /// но и при уменьшении максимального значения.
  final List<List<Size?>> _currentCellSizes = [];

  /// результирующие размеры ячеек, доставляемые ячейкам через нотифаер.
  final List<ValueNotifier<double?>> _columnWidths = [];

  final List<ValueNotifier<double?>> _rowHeights = [ValueNotifier(null)];

  /// зарегистрирован ли колбек на следующий фрейм?
  bool _isPostFrameCallbackRegistered = false;

  /// реагирует на размеры каждой ячейки в колонках
  /// считает оптимальную ширину каждой колонки
  /// [x],[y] – координаты
  /// [size] размеры
  void _onCellSize(int x, int y, Size size) {
    // пропускаем обновления с теми же размерами
    if (_currentCellSizes[x][y] == size) return;
    print('cell ${x}x${y} ${size}');
    _currentCellSizes[x][y] = size;
    if (_isPostFrameCallbackRegistered == false) {
      _isPostFrameCallbackRegistered = true;
      WidgetsBinding.instance?.addPostFrameCallback(_updateCellSizes);
    }
  }

  /// считаем размеры и уведомляем ячейки
  void _updateCellSizes(Duration _) {
    print('_updateCellSizes');
    for (var x = 0; x < _columnWidths.length; x++) {
      _columnWidths[x].value = _getMaxWidth(_currentCellSizes[x]);
    }
    for (var y = 0; y < _rowHeights.length; y++) {
      _rowHeights[y].value = _getMaxHeight(y);
    }
    _isPostFrameCallbackRegistered = false;
  }

  double? _getMaxWidth(List<Size?> sizes) {
    double? result;
    for (final size in sizes) {
      if (size != null && (size.width) > (result ?? -1)) {
        result = size.width;
      }
    }
    return result;
  }

  double? _getMaxHeight(int y) {
    double? result;
    for (var x = 0; x < _currentCellSizes.length; x++) {
      final size = _currentCellSizes[x][y];
      if (size != null && (size.height) > (result ?? -1)) {
        result = size.height;
      }
    }
    return result;
  }

  void _ensureListLength<N>(List<N> list, length, N Function(int) filler) {
    while (list.length < length) {
      list.add(filler(list.length));
    }
    while (list.length > length) {
      final vn = list.removeAt(list.length - 1);
      if (vn is ValueNotifier) vn.dispose();
    }
  }

  void _init(int x, int y) {
    // проверяем, что массивы ширин и высот столбцов/строк имеют нужную длину
    _ensureListLength<ValueNotifier<double?>>(
        _columnWidths, x, (_) => ValueNotifier(null));
    _ensureListLength<ValueNotifier<double?>>(
        _rowHeights, y, (_) => ValueNotifier(null));
    // заполняем двумерный массив размеров каждой ячейки
    _ensureListLength<List<Size?>>(_currentCellSizes, x, (_) => []);
    for (final yList in _currentCellSizes) {
      _ensureListLength<Size?>(yList, y, (_) => null);
    }
  }

  @override
  void dispose() {
    for (final vn in _columnWidths) {
      vn.dispose();
    }
    for (final vn in _rowHeights) {
      vn.dispose();
    }
    super.dispose();
  }
}
