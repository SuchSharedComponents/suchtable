import 'package:flutter/material.dart';

import '../../such_table.dart';
import '../cells/cell.dart';

class SuchTableRow extends StatelessWidget {
  const SuchTableRow({
    required this.rowData,
    required this.onCellSize,
    required this.columnWidths,
    required this.height,
    required this.y,
    required this.generalStyle,
    Key? key,
  }) : super(key: key);

  final SuchRowData rowData;

  final int y;

  final List<ValueNotifier<double?>> columnWidths;
  final ValueNotifier<double?> height;

  /// общий стиль ячеек таблицы для данной строчки
  final SuchContainerStyle generalStyle;

  /// функция, реагирующая на изменения размеров ячеек
  final Function(int x, int y, Size size) onCellSize;

  @override
  Widget build(BuildContext context) {
    final cells = rowData.cells;
    final rowStyle = rowData.cellStyle != null
        ? rowData.cellStyle! | generalStyle
        : generalStyle;
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        for (var i = 0; i < cells.length; i++)
          SuchCell(
            width: columnWidths[i],
            height: height,
            data: cells[i],
            cellStyle: rowStyle,
            onCellSize: onCellSize,
            x: i,
            y: y,
          )
      ],
    );
  }
}
