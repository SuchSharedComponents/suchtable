import 'package:flutter/material.dart';

import '../../such_table.dart';

/// данные ячейки таблицы SuchTable
class SuchRowData {
  const SuchRowData({required this.cells, this.cellStyle});

  final List<SuchCellData> cells;

  /// данные для замены оформления стиля конкретной строчки
  final SuchContainerStyle? cellStyle;
}
