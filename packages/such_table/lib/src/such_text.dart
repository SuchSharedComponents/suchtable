import 'package:flutter/material.dart';

import 'container_style.dart';

/// выводит текст SelectableText/Text виджетом
/// если [text] == null, выводит ø (null sign)
class SuchText extends StatelessWidget {
  const SuchText(this.text, {required this.style, Key? key}) : super(key: key);

  final String? text;

  /// стиль для текста
  final SuchContainerStyle style;

  @override
  Widget build(BuildContext context) {
    return style.selectableText == true
        ? SelectableText(
            text ?? style.nullPlaceholder,
            style: style.textStyle,
            textAlign: style.textAlign,
          )
        : Text(
            text ?? style.nullPlaceholder,
            style: style.textStyle,
            textAlign: style.textAlign,
          );
  }
}
