// файл с параметрами и выбором layout

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:such_table/such_table.dart';

import 'desktop_layout.dart';

/// таблица для вывода данных с сортировкой и поиском
class SuchTable<T> extends HookWidget {
  const SuchTable(this.name,
      { // параметры
      this.isLoading = false,
      this.data = const [],
      this.shimmerRows = 10,
      this.rowBuilder,
      this.showHeader = true,
      this.stickyHeader = false,
      this.headers = const [],
      this.height = 400.0,
      this.mobileBreakpoint,
      this.cellStyle = const SuchContainerStyle(
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 5)),
      this.headerStyle,
      Key? key})
      : super(key: key);

  /// Уникальное имя таблицы. Нигде не выводится.
  final String name;

  /// Даные для таблицы загружаются?
  /// [rowBuilder] не будет вызван пока true
  final bool isLoading;

  /// данные для прохода по ним
  final List<T?> data;

  /// функция построения данных строк таблицы по данным из [data]
  /// вызывается только когда [isLoading] == false
  /// Принимает [item] из массива [data] под номером [index]
  final SuchRowData? Function(T item, int index)? rowBuilder;

  /// показывать ли заголовок таблицы
  final bool showHeader;

  /// оставлять видимым заголовок таблицы при скроллинге
  final bool stickyHeader;

  /// список заголовков столбцов таблицы
  final List<SuchCellData> headers;

  /// Высота таблицы. (игнорируется в [mobileLayout])
  final double height;
  final int shimmerRows;

  /// если установлено значение ≠ null (например: 400.0),
  /// то когда ширина контейнера с таблицей ≤ [mobileBreakpoint]
  /// будет использовано мобильное представление
  /// когда null, всегда будет использоваться desktopLayout
  final double? mobileBreakpoint;

  /// стиль каждой ячейки
  final SuchContainerStyle cellStyle;

  /// отдельные параметры для ячеек заголовка (иначе используются параметры обычных ячеек)
  final SuchContainerStyle? headerStyle;

  @override
  Widget build(BuildContext context) {
    if (kDebugMode) {
      if (debugCheckHasMaterialLocalizations(context) == false) {
        print('Не найден MaterialLocalizations в контексте');
      }
    }

    return LayoutBuilder(
      builder: (context, constraints) {
        if (mobileBreakpoint != null &&
            constraints.maxWidth <= mobileBreakpoint!) {
          // return mobile layout
        }
        return SuchTableDesktopLayout<T>(
          headers: headers,
          data: data,
          isLoading: isLoading,
          rowBuilder: rowBuilder,
          stickyHeader: stickyHeader,
          availableWidth: constraints.maxWidth,
          cellStyle: cellStyle,
          headerStyle: headerStyle,
          shimmerRows: shimmerRows,
          height: height,
        );
      },
    );
  }
}
