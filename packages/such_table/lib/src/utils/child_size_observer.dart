// v0.1 (2022-05-14)

import 'package:flutter/material.dart';

/// Получает размеры [child] после окончания построения
/// древа виджетов, ребилдов и изменений ориентации устройства.
/// Возвращает [Size] вместе с произвольным [id] в колбеке [onSize]
/// Если [id] не указан, возвращается [key]
class ChildSizeObserver extends StatelessWidget {
  const ChildSizeObserver({required this.child, required this.onSize, this.id, Key? key}) : super(key: key);

  final Widget child;
  final dynamic id;
  final Function(dynamic, Size) onSize;

  @override
  Widget build(BuildContext context) {
    // ignore: sort_child_properties_last
    return OrientationBuilder(builder: (_, __) => ChildSizeObserverRebuild(child: child, onSize: onSize, id: id, key: key));
  }
}

/// Рекомендуется использовать [ChildSizeObserver] вместо этого виджета.
/// [ChildSizeObserver] оборачивает [ChildSizeObserverRebuild] в [OrientationBuilder],
/// чтобы реагировать на изменения ориентации устройства.
/// Иначе данный наблюдатель реагирует только на ребилды.
class ChildSizeObserverRebuild extends StatefulWidget {
  const ChildSizeObserverRebuild({required this.child, required this.onSize, this.id, Key? key}) : super(key: key);

  final Widget child;
  final dynamic id;
  final Function(dynamic, Size) onSize;

  @override
  _ChildSizeObserverRebuildState createState() => _ChildSizeObserverRebuildState();
}

class _ChildSizeObserverRebuildState extends State<ChildSizeObserverRebuild> {
  @override
  void initState() {
    _onResize();
    super.initState();
  }

  void _onResize() {
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      if (context.size is Size) {
        widget.onSize(widget.id ?? widget.key, context.size!);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    _onResize();
    return widget.child;
  }
}
