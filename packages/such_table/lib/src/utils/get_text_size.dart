import 'package:flutter/material.dart';

import '../../such_table.dart';

/// Считает занимаемый текстом размер с учетом стиля.
/// Следует задать [maxLines] и [maxWidth] для многострочного поля.
Size getTextSize(String text, TextStyle? style, {int? maxLines = 1, double? maxWidth = double.infinity}) {
  final TextPainter textPainter =
      TextPainter(text: TextSpan(text: text, style: style), maxLines: maxLines, textDirection: TextDirection.ltr)
        ..layout(maxWidth: maxWidth ?? double.infinity);
  return textPainter.size;
}

/// считает размеры контейнера с текстом со стилями из класса [SuchContainerStyle]
Size getContainerSizeWithText(String text, SuchContainerStyle containerStyle, {bool withPadding = true}) {
  final textSize = getTextSize(text, containerStyle.textStyle, maxLines: 1, maxWidth: containerStyle.maxWidth);
  if (withPadding == false) return textSize;
  final _wide = (containerStyle.padding?.horizontal ?? .0) + (containerStyle.decoration?.border?.dimensions.horizontal ?? .0);
  final _height = (containerStyle.padding?.vertical ?? .0) + (containerStyle.decoration?.border?.dimensions.vertical ?? .0);
  return Size(textSize.width + _wide, textSize.height + _height);
}
